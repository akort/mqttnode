var mqtt = require("mqtt")
var url = "mqtts://localhost"
var rnd = new Date().getTime()
var client;
var redis = require("redis")
var redisClient = redis.createClient()
var options = {
	clientId: "nodeclient"+rnd,
	port: 8443,
	keepalive: 60,
	certPath: "cert.cer",
	keyPath: "key.pem"
}

// fixme
/*
var i;
for (i =0;i < 10; i++) {
	options.clientId = Math.random()
	client = mqtt.connect(url,options)
	client.on("connect", onConnect);
client.on("message",onMessage);

}
*/
client = mqtt.connect(url,options)

client.on("connect", onConnect);
client.on("message",onMessage);

function onConnect() {
	client.subscribe("timestamp",mqttSubscribe)
	client.subscribe("temp_avg",mqttSubscribe)
	client.unsubscribe("temp_avg",mqttUnsub)// test unsubscribe
}

function onMessage(topic,message) {
	console.log(topic + " " +message.toString())
	// not pretty but saves them into redis kv-store
	redisClient.set(topic+ " " +(Math.random()*1000).toFixed(3)
	,message,redis.print)
}
function mqttSubscribe() {
	console.log("subscribed ")
}

function mqttUnsub() {
	console.log("unsubbed")
}