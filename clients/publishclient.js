var mqtt = require("mqtt")
var url = "mqtts://localhost"
var rnd = new Date().getTime()
var client;
var options = {
	clientId: "nodepublish"+rnd,
	port: 8443,
	keepalive: 60,
	certPath: "cert.cer",
	keyPath: "key.pem",
}

// fixme
/*
var i;
for (i =0;i < 10; i++) {
	options.clientId = Math.random()
	client = mqtt.connect(url,options)
	client.on("connect", onConnect);
client.on("message",onMessage);

}
*/
client = mqtt.connect(url,options)

client.on("connect", onConnect);
client.on("message",onMessage);

function onConnect() {
	client.publish("timestamp","this is a message from publishjs"+rnd)
}

function onMessage(topic,message) {
	console.log(topic + " " +message.toString())
	//client.end()
}
function mqttSubscribe() {
	console.log("subscribed ")
}

function mqttUnsub() {
	console.log("unsubbed")
}