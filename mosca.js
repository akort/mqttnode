var mosca = require("mosca")

var redis = {
	type: 'redis',
	redis: require('redis'),
	db: 12,
	port: 6379,
	return_buffers: true, // to handle binary payloads
	host: "localhost"
  };

var moscaSettings = {
	port:1884,
	backend: redis,
	persistence: {
		factory: mosca.persistence.Redis
	},
	secure: {
		port:8443,
		keyPath:"key.pem",
		certPath:"cert.pem"
	}
}

var server = new mosca.Server(moscaSettings)
server.on("ready",setup)

server.on("clientConnected",function (client) {
	console.log("client connected with: " + client.id)

})

var message = {
	topic:"timestamp",
	payload: "hello",
	qos:0
}

var messageToUnsub = {
	topic:"temp_avg",
	payload: "This shouldnt be delivered",
	qos:0
}


// for simplicity we send a message to client when a client subscribes
// each time a client subscribes, the message is published (and saved into db)
server.on("subscribed",function(topic) {
	console.log("CLIENT SUBSCRIBED IN: "+topic)
	message.topic=topic
	server.publish(message, function () {
		//console.log("publsihed message")
		//message.topic+message.payload.toString())
	})
	server.publish(messageToUnsub,function() {
		//console.log("this should be een on client")
	})
})

server.on("unsubscribed",function(topic){
	console.log("Unsubbed in "+topic)
	//client.unsubscribe
})

server.on("published", function(packet, client) {
	console.log("Published", packet.topic, packet.payload.toString());
  });
  
  server.on('clientDisconnected', function(client) {
	console.log("client disconnecteds: ", client.id);
	});


  function setup() {
	console.log('Mosca server is up and running')
	
  }