get keys


openssl req -x509 -newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost'   -keyout key.pem -out cert.pem


https://github.com/mokjpn/hubot-mqtt/blob/master/doc/easy-rsa

publishclient can be used to create more publishers

start redis: redis-server

start the server: node mosca

start a client: node mqtt

(start a publisher to test client: node publishclient)

redis is used because it scales well, and values are never really accessed so the keys can be just random strings

redis desktop manager can be used to view the database

FLUSHDB to delete data in the db